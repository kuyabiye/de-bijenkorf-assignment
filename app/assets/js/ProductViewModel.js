define(["ko"], function(ko) {
    "use strict";

   function Product(item, _parent) {
    var self = this;
    this.checked = ko.observable(false);
    this.sku = item.sku;
    this.name = item.name;
    this.image = item.image;
    this.brand = item.brand;
    this.description = item.description;
    this.quantity = item.quantity;
    this.color = item.color;
    this.size = item.size;
    this.price = item.price;
    this._parent = _parent;
  }  

  return Product;
});

