define(["jquery", "ko", "BasketViewModel"], function($, ko, BasketViewModel) {

  function Basket(view, options) {
    var self = this;

    this.view = view;
    this.options = $.extend({}, this.constructor.DEFAULTS, options);

    this.init();
  }

  Basket.prototype.init = function() {  
    ko.applyBindings(BasketViewModel(), this.view);       
  }

  Basket.DEFAULTS = {};

  return Basket;

});