define(["ko"], function(ko) {

  function UserViewModel(user) {
    var self = this;
   
    this.firstName = ko.observable(user.firstName);
    this.lastName = ko.observable(user.lastName);
    this.gender = ko.observable(user.gender);
    this.telephone = ko.observable(user.telephone);
    this.dob = ko.observable(user.dob);

    this.street = ko.observable(user.address.street);
    this.number = ko.observable(user.address.number);
    this.postcode = ko.observable(user.address.postcode);
    this.city = ko.observable(user.address.city);
    
    this.title = ko.computed(function() {
      var gender = this.gender();
      if ( gender === "male" ) {
        return "Mr.";
      } else if ( gender === "female" ) {
        return "Mrs.";
      } else {
        return "n/a";
      }
    }, this);

    this.fullname = ko.computed(function() {
      return this.title() + " " + this.firstName() + " " + this.lastName();
    }, this);


    this.address = ko.computed(function() {
      return [this.number(), this.street(), this.city(), this.postcode()].join(" ");
    }, this);
  }

  return UserViewModel;
});