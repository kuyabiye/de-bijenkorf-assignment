define(["ko", "View", "BasketViewModel", "ProductViewModel",  "text!templates/wishlists.html"], function(ko, View, BasketViewModel, ProductViewModel, template) {

  function WishlistViewModel(options) {
    var self = this;

    View.call(this, options, template);

    this.getItems = function() {
      var request = new XMLHttpRequest(); 
      request.open("GET", self.options.api.get, true);
      
      request.onreadystatechange = function () {
      
          if ( request.readyState !== 4 || request.status !== 200 ) {
              return; 
          }

          self.items.removeAll();

          var data = JSON.parse(this.response);
          for (var i = 0, len = data.items.length; i < len; i++) {
              self.items.push(new ProductViewModel(data.items[i], self));
          }

      };

      request.send();             
    }

    this.toggleAll = ko.observable(false);

    this.toggleAll.subscribe(function(value) {
      var items = self.items();
      for ( var i = 0, len = items.length; i < len; i++ ) {
        items[i].checked(value);
      }
    });

    this.items = ko.observableArray();
    this.getItems();

    this.removeItem = function(item) {
      request = new XMLHttpRequest(); 
      request.open("DELETE", "/api/wishlists/" + this.sku);
      
      request.onreadystatechange = function () {
      
          if ( request.readyState !== 4 || request.status !== 200 ) {
              return; 
          }

          self.items.remove(item);
      };

      request.send();    
    }

    this.addBasket = function(item) {
      BasketViewModel().add(item);
    }

    this.addSelectedToBasket = function() {
      var items = self.items();

      for ( var i = 0, len = items.length; i < len; i++ ) {
        if ( items[i].checked() ) {
          self.addBasket(items[i]);
          items[i].checked(false);
        }
      }
    }

    this.removeAll = function() {
      request = new XMLHttpRequest(); 
      request.open("DELETE", "/api/wishlists");
      
      request.onreadystatechange = function () {
      
          if ( request.readyState !== 4 || request.status !== 200 ) {
              return; 
          }

          self.items.removeAll();
      };

      request.send();    
    }

    this.sendEmail = function() {
      var items = self.items(),
        content = "";

      content = items.map(function(item) {
       return content + item.name + "\n";
      });
       
      alert(content);
      
    }
    
    this.init();    
  }

  WishlistViewModel.prototype = Object.create(View.prototype);
  WishlistViewModel.prototype.constructor = WishlistViewModel;
  
  WishlistViewModel.DEFAULTS = {
    "selector": ".js-wishlists",
    "api": {
      "get": "/api/wishlists",
      "delete": "/api/wishlists/:sku"
    }
  }

  return WishlistViewModel;
});
