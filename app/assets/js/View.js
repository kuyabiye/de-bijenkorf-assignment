define(["jquery", "ko"], function($, ko) {

  function View(options, template) {
    this.options = $.extend({}, this.constructor.DEFAULTS, options);
    this.template = template;
  }

  View.prototype.init = function(ViewModel) {
    var context = document.querySelector(".js-content");
    context.innerHTML = this.template;
    this.view = context.querySelector(this.options.selector);

    ko.applyBindings(ViewModel || this, this.view);       
  }

  View.prototype.destroy = function() {
    this.view = null;

    context.innerHTML = "";
  }

  return View;

});