define(["jquery", "ko", "View", "user", "text!templates/my-details.html"], function($, ko, View, user, template) {

  function UserDetails(options) {
    var self = this;

    View.call(this, options, template);

    this.init(user);
  }

  UserDetails.prototype = Object.create(View.prototype);
  UserDetails.prototype.constructor = UserDetails;

  UserDetails.DEFAULTS = {
    selector: ".js-user-details"
  }

  return UserDetails;
});