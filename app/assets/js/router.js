define(["crossroads", "hasher"], function(crossroads, hasher) {
   crossroads.addRoute("/", function() {
      crossroads.parse("/wishlist");
   })

  crossroads.addRoute("/wishlist")
  .matched.add(function() {
    require(["WishlistViewModel"], function(WishlistViewModel) {
      new WishlistViewModel();
    });
  });


  crossroads.addRoute("/my-details")
  .matched.add(function() {
   require(["UserDetails"], function(UserDetails) {
      new UserDetails();
    });
  });
    
    
  //setup hasher
  function parseHash(newHash, oldHash){
    crossroads.parse(newHash);
  }
  hasher.initialized.add(parseHash);
  hasher.changed.add(parseHash);
  hasher.init(); 

  return {
    setRoute: function(url) {
      crossroads.parse(url);
    }
  }   
});