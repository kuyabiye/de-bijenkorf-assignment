define(["UserViewModel"], function(UserViewModel) {
  
  return new UserViewModel({
   "firstName": "Joe",
   "lastName": "Bloggs",
   "gender": "male",
   "telephone":"0612345678",
   "dob" : "1982-01-15",
   "address": {
      "street":"Dam",
      "number":"1",
      "postcode":"1012JS",
      "city":"Amsterdam"
   }});
});