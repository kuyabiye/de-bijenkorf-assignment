define(["jquery", "ko", "ProductViewModel"], function($, ko, ProductViewModel) {
  var instance;

  function BasketViewModel() {
    var self = this;
    this.items = ko.observableArray([]);

    this.add = function(item) {
      this.items.push(new ProductViewModel(item, self))
    }

    this.total = ko.computed(function() {
      var items = self.items(), 
        total = 0;

      for ( var i = 0, len = items.length; i < len; i++ ) {
        total += parseFloat(items[i].price);
      }

      return total.toFixed(2);
    });

    this.getItems = function() {
      // get items from db.
    }

    this.getItems();
  }

  return function() {
    instance = instance || new BasketViewModel();
    return instance;
  }

});