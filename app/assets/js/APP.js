'use strict';

require.config({
  paths: {
    jquery: '../../node_modules/jquery/dist/jquery.min',
    ko: '../../node_modules/knockout/build/output/knockout-latest',
    signals: '../../node_modules/signals/dist/signals.min',
    crossroads: '../../node_modules/crossroads/dist/crossroads.min',
    hasher: '../../node_modules/hasher/dist/js/hasher.min',
    text: '../../node_modules/text/text'
  }
});

var APP = APP || {};

APP.init = function (context) {
  var _context = context || document.body;

  var views = _context.querySelectorAll(".js-module");

  [].forEach.call(views, function(view) {
    var data = view.dataset;
    if ( data.model ) {
      require([data.model], function(Model) {
        new Model(view, view.dataset || {});
      })
    };
  });

}


require(["router"], function(router) {
  APP.init();
});