## Setup:

* Install [Node.JS](http://nodejs.org/)
* In this directory, run `npm install`
* Start the server: `node server`
* Visit [http://localhost:3001](http://localhost:3001)
